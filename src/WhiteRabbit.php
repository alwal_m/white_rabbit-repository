<?php

class WhiteRabbit
{
    public $arrayOfLetters = array('a'=> 0, 'b'=> 0,
       'c'=> 0, 'd'=> 0, 'e'=> 0, 'f'=> 0, 'g'=> 0, 
       'h'=> 0, 'i'=> 0, 'j'=> 0, 'k'=> 0, 'l'=> 0, 
       'm'=> 0, 'n'=> 0, 'o'=> 0, 'p'=> 0, 'q'=> 0, 
       'r'=> 0, 's'=> 0, 't'=> 0, 'u'=> 0, 'v'=> 0, 
       'w'=> 0, 'x'=> 0, 'y'=> 0, 'z'=> 0 );
       
      public $occurrences = array();

    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        if(file_exists($file))
               {
                 $string = file_get_contents($file); 
                 $contentAsString = str_split($string);
                  return $lowerCaseString = strtolower($contentAsString);
               }
               else
               {
                  break;
               }

    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        foreach($i = 0; $i<=strlen($parsedFile); $i++)
       {
         $char = $parsedFile[$i];
         if(array_key_exists($char, $arrayOfLetters))
         {
            $indexOfKey = array_search($char, array_keys($arrayOfLetters));
            $occurrences[$indexOfKey]++;
         }
       }
       $medianLettervalue = (sizeof($occurrences)+1)/2;
      return $arrayOfLetters[$medianLettervalue];

    }
}

$newClass = new WhiteRabbit();
$newClass->findMedianLetterInFile($filePath);

